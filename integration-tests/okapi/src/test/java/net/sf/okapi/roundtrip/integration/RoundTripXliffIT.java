package net.sf.okapi.roundtrip.integration;

import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.integration.EventRoundTripIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.xliff.XLIFFFilter;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class RoundTripXliffIT extends EventRoundTripIT {
	private static final String CONFIG_ID = "okf_xliff";
	private static final String DIR_NAME = "/xliff/";
	private static final List<String> EXTENSIONS = Arrays.asList(".xliff", ".xlf");

	final static FileLocation root = FileLocation.fromClass(RoundTripXliffIT.class);

	public RoundTripXliffIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS);
		addKnownFailingFile("DE_CALC_PHASE1.xlsx.sdlxliff");
		addKnownFailingFile("ImplementationPlan.docx.xlf");
		addKnownFailingFile("lqiTest.xlf");
		addKnownFailingFile("sampe_sch.xliff");
		addKnownFailingFile("test.txt.xlf");
		addKnownFailingFile("DifferentTags.xlf");
	}

	@Before
	public void setUp() throws Exception {
		filter = new XLIFFFilter();
	}

	@After
	public void tearDown() throws Exception {
		filter.close();
	}

	@Ignore
	@Test
	public void debug() throws FileNotFoundException, URISyntaxException {
		setConfigId("okf_xliff-sdl");
		final File file = root.in("/xliff/sdlxliff/DLG_createEstimate.html.sdlxliff").asFile();
		runTest(true, file, null, null, new FileComparator.EventComparator());
	}

	@Test
	public void sdlXliff() throws FileNotFoundException, URISyntaxException {
		setConfigId("okf_xliff-sdl");
		setExtensions(Arrays.asList(".sdlxliff"));
		realTestFiles(true, new FileComparator.EventComparator());
	}

	@Test
	public void worldserverXliff() throws FileNotFoundException, URISyntaxException {
		setConfigId("okf_xliff-iws");
		setExtensions(Arrays.asList(".iwsxliff"));
		realTestFiles(true, new FileComparator.EventComparator());
	}

	@Test
	public void xliffFiles() throws FileNotFoundException, URISyntaxException {
		setConfigId(CONFIG_ID);
		setExtensions(EXTENSIONS);
		realTestFiles(true, new FileComparator.EventComparatorIgnoreSegmentation());
	}
}
