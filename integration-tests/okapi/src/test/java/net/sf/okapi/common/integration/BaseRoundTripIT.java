/**
 * 
 */
package net.sf.okapi.common.integration;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.filters.IFilter;
import org.junit.Rule;
import org.junit.rules.ErrorCollector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author jimh
 *
 */
public abstract class BaseRoundTripIT {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	final String defaultConfigId;
	protected String configId;
	final String dirName;
	List<String> extensions;
	final LocaleId defaultTargetLocale;
	final Set<String> knownFailingFiles = new HashSet<>();
	final String xliffExtractedExtension;
	protected IFilter filter;

	@Rule
	public ErrorCollector errCol = new ErrorCollector();

	public BaseRoundTripIT(final String configId, final String dirName, final List<String> extensions) {
		this(configId, dirName, extensions, LocaleId.FRENCH);
	}

	public BaseRoundTripIT(final String configId, final String dirName, final List<String> extensions, final LocaleId defaultTargetLocale) {
		this.defaultConfigId = configId;
		this.configId = configId;
		this.dirName = dirName;
		this.extensions = extensions;
		this.defaultTargetLocale = defaultTargetLocale;
		xliffExtractedExtension = configId.startsWith("okf_xliff") ? ".xliff_extracted" : ".xliff";
	}

	public void setConfigId(String configId) {
		this.configId = configId;
	}

	public void setExtensions(List<String> extensions) {
		this.extensions = extensions;
	}

	public void addKnownFailingFile(final String fileName) {
		knownFailingFiles.add(fileName);
	}

	public void realTestFiles(final boolean detectLocales, final IComparator comparator)
			throws FileNotFoundException, URISyntaxException {

		// run top level files (without config)
		File dir = IntegrationtestUtils.ROOT.in(dirName).asFile();
		for (final File file : IntegrationtestUtils.getTestFilesNoRecurse(dir, extensions)) {
			runTest(detectLocales, file, null, null, comparator);
		}

		// run each subdirectory where we assume there is a custom config)
		for (final File subDir : IntegrationtestUtils.getSubDirsNoRecurse(dir)) {
			for (final File file : IntegrationtestUtils.getTestFilesNoRecurse(subDir, extensions)) {
				Collection<File> configs = IntegrationtestUtils.getConfigFile(subDir);
				if (configs.isEmpty()) {
					setConfigId(defaultConfigId);
					runTest(detectLocales, file, subDir, null, comparator);
				} else {
					for (final File c : configs) {
						setConfigId(Util.getFilename(c.getAbsolutePath(), false));
						final String customConfigPath = c.getParent();
						runTest(detectLocales, file, subDir, customConfigPath, comparator);
					}
				}
			}
		}
	}

	abstract protected void runTest(final boolean detectLocales, final File file, File subDir,
									final String customConfigPath, final IComparator comparator);
}
