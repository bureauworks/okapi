/*===========================================================================
  Copyright (C) 2021 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.applications;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.event.Level;

@RunWith(JUnit4.class)
public class OkapiLoggerTest {
	Logger logger;
	LogToWriterAppender appender;

	@Before
	public void SetUp() {
		logger = LoggerFactory.getLogger(getClass());
		appender = new LogToWriterAppender();
		OkapiLogger.setAppender(appender);
	}

	@Test
	public void testDefaults() {
		assertTrue(logger.isErrorEnabled());
		assertTrue(logger.isWarnEnabled());
		assertTrue(logger.isInfoEnabled());
		assertFalse(logger.isTraceEnabled());
		assertFalse(logger.isDebugEnabled());
	}

	@Test
	public void testNoParameter() {
		logger.info("This is logging at info level");
		String logOutput = appender.getOutput();
		assertTrue(logOutput.startsWith("[INFO]"));
		assertTrue(logOutput.contains("at info level"));
	}

	@Test
	public void testOneParameter() {
		logger.info("This is logging at info level, parameter: {}", 42);
		String logOutput = appender.getOutput();
		assertTrue(logOutput.startsWith("[INFO]"));
		assertTrue(logOutput.contains("at info level"));
		assertTrue(logOutput.contains("parameter: 42"));
	}

	@Test
	public void testParameterArray() {
		logger.info("This is logging at info level, parameters: {}={}, {}={}", 1, "one", 2, "two");
		String logOutput = appender.getOutput();
		assertTrue(logOutput.startsWith("[INFO]"));
		assertTrue(logOutput.contains("at info level"));
		assertTrue(logOutput.contains("parameters: 1=one, 2=two"));
	}

	@Test
	public void testLogWithException() {
		String exceptionMessage = "Simple NPE with message";
		Exception e = new NullPointerException(exceptionMessage);
		logger.info("This is logging at info level, with exception", e);
		String logOutput = appender.getOutput();
		assertTrue(logOutput.startsWith("[INFO]"));
		assertTrue(logOutput.contains("at info level"));
		assertTrue(logOutput.contains("with exception"));
		assertTrue(logOutput.contains(exceptionMessage));
	}

	@Test
	public void testLevelsWork() {
		// Default level should be info
		String logOutput = logAtAllLevels();
		assertTrue(logOutput.contains("at error level"));
		assertTrue(logOutput.contains("at warn level"));
		assertTrue(logOutput.contains("at info level"));
		assertFalse(logOutput.contains("at debug level"));
		assertFalse(logOutput.contains("at trace level"));
	}

	@Test
	public void testLevelsWorkTrace() {
		appender.setLevel(Level.TRACE);
		String logOutput = logAtAllLevels();
		assertTrue(logOutput.contains("at error level"));
		assertTrue(logOutput.contains("at warn level"));
		assertTrue(logOutput.contains("at info level"));
		assertTrue(logOutput.contains("at debug level"));
		assertTrue(logOutput.contains("at trace level"));
	}

	@Test
	public void testLevelsDebug() {
		appender.setLevel(Level.DEBUG);
		String logOutput = logAtAllLevels();
		assertTrue(logOutput.contains("at error level"));
		assertTrue(logOutput.contains("at warn level"));
		assertTrue(logOutput.contains("at info level"));
		assertTrue(logOutput.contains("at debug level"));
		assertFalse(logOutput.contains("at trace level"));
	}

	@Test
	public void testLevelsWarn() {
		appender.setLevel(Level.WARN);
		String logOutput = logAtAllLevels();
		assertTrue(logOutput.contains("at error level"));
		assertTrue(logOutput.contains("at warn level"));
		assertFalse(logOutput.contains("at info level"));
		assertFalse(logOutput.contains("at debug level"));
		assertFalse(logOutput.contains("at trace level"));
	}

	@Test
	public void testLevelsError() {
		appender.setLevel(Level.ERROR);
		String logOutput = logAtAllLevels();
		assertTrue(logOutput.contains("at error level"));
		assertFalse(logOutput.contains("at warn level"));
		assertFalse(logOutput.contains("at info level"));
		assertFalse(logOutput.contains("at debug level"));
		assertFalse(logOutput.contains("at trace level"));
	}

	private String logAtAllLevels() {
		logger.error("This is logging at error level");
		logger.warn("This is logging at warn level");
		logger.info("This is logging at info level");
		logger.debug("This is logging at debug level");
		logger.trace("This is logging at trace level");
		return appender.getOutput();
	}
}
