/*===========================================================================
  Copyright (C) 2021 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.applications;

import java.io.CharArrayWriter;

import org.slf4j.event.Level;

/**
 * Logs to a CharArrayWriter so that we can look at the logging results for testing.
 */
public class LogToWriterAppender implements IOkapiLoggerAppender {
	private Level level = Level.INFO;
	private CharArrayWriter writer = new CharArrayWriter();

	@Override
	public void log(String loggerName, Level level, String message, Throwable throwable) {
		writer.append(String.format("[%s] {%s} : %s.", level, loggerName, message));
		if (throwable != null) {
			writer.append(String.format(" Exception: %s", throwable.getMessage()));
		}
		writer.append(String.format("%n"));
	}

	@Override
	public Level getLevel() {
		return level;
	}

	public void setLevel(Level level) {
		this.level = level;
	}

	public void reset() {
		writer.reset();
	}

	public String getOutput() {
		writer.flush();
		return writer.toString();
	}
}
