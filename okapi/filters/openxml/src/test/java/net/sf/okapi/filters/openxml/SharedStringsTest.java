package net.sf.okapi.filters.openxml;

import java.util.ArrayList;
import java.util.List;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class SharedStringsTest {

	@Test
	public void reordered() {
		SharedStrings ssm = new SharedStrings();
		ssm.add(new SharedStrings.Item(4, ssm.nextIndex(), "w", "1", "A", true));
		ssm.add(new SharedStrings.Item(1, ssm.nextIndex(), "w", "1", "B", true));
		ssm.add(new SharedStrings.Item(3, ssm.nextIndex(), "w", "1", "C", true));
		ssm.add(new SharedStrings.Item(2, ssm.nextIndex(), "w", "1", "D", true));
		List<SharedStrings.Item> expected = new ArrayList<>();
		expected.add(new SharedStrings.Item(4, 0, "w", "1", "A", true));
		expected.add(new SharedStrings.Item(1, 1, "w", "1", "B", true));
		expected.add(new SharedStrings.Item(3, 2, "w", "1", "C", true));
		expected.add(new SharedStrings.Item(2, 3, "w", "1", "D", true));
		assertEquals(expected, ssm.items());
	}
}
