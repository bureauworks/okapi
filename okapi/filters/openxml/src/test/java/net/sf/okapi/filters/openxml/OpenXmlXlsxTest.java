package net.sf.okapi.filters.openxml;

import static net.sf.okapi.filters.openxml.CodePeekTranslator.locENUS;
import static net.sf.okapi.filters.openxml.OpenXMLTestHelpers.textUnitSourceExtractor;
import static org.assertj.core.api.Assertions.assertThat;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.resource.Ending;
import net.sf.okapi.common.resource.INameable;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.common.resource.StartGroup;
import net.sf.okapi.common.resource.TextUnit;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.iterable.Extractor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * @author jpmaas
 * @since 17.08.2017
 */
@RunWith(JUnit4.class)
public class OpenXmlXlsxTest {

	private FileLocation root;

	@Before
	public void setUp() {
		root = FileLocation.fromClass(getClass());
	}

    @Test
    public void testTextFields() throws Exception {
        ConditionalParameters parameters = new ConditionalParameters();
        parameters.setTranslateExcelDrawings(true);
        parameters.setTranslateDocProperties(false);
        List<Event> actual = eventsFor("/textfield.xlsx", parameters);
        List<ITextUnit> textUnits = FilterTestDriver.filterTextUnits(actual);
        assertThat(textUnits).extracting((Extractor<ITextUnit, Object>) input -> input.getSource().toString()).containsExactly(
                "Hallo Welt!",
                "Ich bin ein Textfeld!");
    }

    @Test
    public void testExcelWorksheetTransUnitProperty() throws Exception {
        ConditionalParameters parameters = new ConditionalParameters();
        parameters.setTranslateExcelDrawings(true);
        parameters.setTranslateDocProperties(false);
        List<Event> actual = eventsFor("/textfield.xlsx", parameters);
        List<ITextUnit> textUnits = FilterTestDriver.filterTextUnits(actual);
        assertThat(textUnits).extracting((Extractor<ITextUnit, Object>) input -> input.hasProperty(
            ExcelWorksheetTransUnitProperty.CELL_REFERENCE.getKeyName())).containsExactly(
            true,
            false);
        assertThat(textUnits).extracting((Extractor<ITextUnit, Object>) input -> String.valueOf(input.getProperty(
            ExcelWorksheetTransUnitProperty.CELL_REFERENCE.getKeyName()))).containsExactly(
            "A1",
            "null");
        assertThat(textUnits).extracting((Extractor<ITextUnit, Object>) input -> String.valueOf(input.getProperty(
            ExcelWorksheetTransUnitProperty.SHEET_NAME.getKeyName()))).containsExactly(
            "Tabelle1",
            "null");
        assertThat(textUnits).extracting((Extractor<ITextUnit, Object>) INameable::getName).containsExactly(
            "Tabelle1!A1",
            null);
    }

    @Test
    public void testSmartArt() throws Exception {
        ConditionalParameters parameters = new ConditionalParameters();
        parameters.setTranslateExcelDiagramData(true);
        parameters.setTranslateDocProperties(false);
        List<Event> actual = eventsFor("/smartart.xlsx", parameters);
        List<ITextUnit> textUnits = FilterTestDriver.filterTextUnits(actual);
        assertThat(textUnits).extracting((Extractor<ITextUnit, Object>) input -> input.getSource().toString()).containsExactly(
            "Hallo Welt!", "Ich", "bin", "ein", "Smart", "Art"
        );
    }

    @Test
    public void testSmartArtHidden() throws Exception {
        ConditionalParameters parameters = new ConditionalParameters();
        parameters.setTranslateExcelDiagramData(true);
        parameters.setTranslateDocProperties(false);
        parameters.setTranslateExcelHidden(false);
        List<Event> actual = eventsFor("/SmartArt3Sheets.xlsx", parameters);
        List<ITextUnit> textUnits = FilterTestDriver.filterTextUnits(actual);
        assertThat(textUnits).extracting((Extractor<ITextUnit, Object>) input -> input.getSource().toString()).containsExactly(
            "Zelle 1", "Zelle 3", "Smart Art 1", "Smart Art 3"
        );
    }

    @Test
    public void testTextFieldsHidden() throws Exception {
        ConditionalParameters parameters = new ConditionalParameters();
        parameters.setTranslateExcelDrawings(true);
        parameters.setTranslateDocProperties(false);
        parameters.setTranslateExcelHidden(false);
        List<Event> actual = eventsFor("/Textfeld3Sheets.xlsx", parameters);
        List<ITextUnit> textUnits = FilterTestDriver.filterTextUnits(actual);
        assertThat(textUnits).extracting((Extractor<ITextUnit, Object>) input -> input.getSource().toString()).containsExactly(
                "Zelle 1",
                "Zelle 3",
                "Textfeld 1",
                "Textfeld 3");
    }

    @Test
    public void testSheetNamesHiddenExclude() throws Exception {
        ConditionalParameters parameters = new ConditionalParameters();
        parameters.setTranslateExcelSheetNames(true);
        List<Event> actual = eventsFor("/SheetNameHidden.xlsx", parameters);
        List<ITextUnit> textUnits = FilterTestDriver.filterTextUnits(actual);
        assertThat(textUnits).extracting((Extractor<ITextUnit, Object>) input -> input.getSource().toString()).containsExactly(
                "Cell Visible",
                "Sheet Visible");
    }

    @Test
    public void testSheetNamesHiddenInclude() throws Exception {
        ConditionalParameters parameters = new ConditionalParameters();
        parameters.setTranslateExcelSheetNames(true);
        parameters.setTranslateExcelHidden(true);
        List<Event> actual = eventsFor("/SheetNameHidden.xlsx", parameters);
        List<ITextUnit> textUnits = FilterTestDriver.filterTextUnits(actual);
        assertThat(textUnits).extracting((Extractor<ITextUnit, Object>) input -> input.getSource().toString()).containsExactly(
                "Cell Visible",
                "Cell Hidden",
                "Sheet Visible",
                "Sheet Hidden"
        );
    }

    @Test
    public void groupsOfWorksheetsAndRowsExtracted() throws Exception {
        final List<Event> actual = eventsFor("/1059.xlsx", new ConditionalParameters());
        Assertions.assertThat(actual.size()).isEqualTo(33);
        Assertions.assertThat(actual.get(5).isStartGroup()).isTrue();
        Assertions.assertThat(actual.get(5).getStartGroup().getName()).isEqualTo("Лист1");
        Assertions.assertThat(actual.get(6).isStartGroup()).isTrue();
        Assertions.assertThat(actual.get(6).getStartGroup().getName()).isEqualTo("2");
        Assertions.assertThat(actual.get(7).isTextUnit()).isTrue();
        Assertions.assertThat(actual.get(8).isTextUnit()).isTrue();
        Assertions.assertThat(actual.get(9).isTextUnit()).isTrue();
        Assertions.assertThat(actual.get(10).isTextUnit()).isTrue();
        Assertions.assertThat(actual.get(11).isEndGroup()).isTrue();
        Assertions.assertThat(actual.get(11).getEndGroup().getId()).isEqualTo(actual.get(6).getStartGroup().getId());
        Assertions.assertThat(actual.get(18).isEndGroup()).isTrue();
        Assertions.assertThat(actual.get(18).getEndGroup().getId()).isEqualTo(actual.get(5).getStartGroup().getId());
    }

    @Test
    public void testFormattings() throws Exception {
        ConditionalParameters params = new ConditionalParameters();
        params.setTranslateDocProperties(false);
        params.setTranslatePowerpointMasters(false);
        List<Event> events = eventsFor("/Formattings.xlsx", params);

        List<ITextUnit> textUnits = FilterTestDriver.filterTextUnits(events);
        assertThat(textUnits).extracting(textUnitSourceExtractor()).containsExactlyInAnyOrder(
                "This is a <run1>bold formatting</run1>",
                "This is an <run1>italics formatting</run1>",
                "This is an <run1>underlined formatting</run1>",
                "This is a hyperlink"
        );

        assertThat(
                textUnits.get(0).getSource().getParts().get(0).getContent().getCodes()
        ).hasSize(2).extracting("type").containsExactly(
                "x-bold;",
                "x-bold;"
        );
        assertThat(
                textUnits.get(1).getSource().getParts().get(0).getContent().getCodes()
        ).hasSize(2).extracting("type").containsExactly(
                "x-italic;",
                "x-italic;"
        );
        assertThat(
                textUnits.get(2).getSource().getParts().get(0).getContent().getCodes()
        ).hasSize(2).extracting("type").containsExactly(
                "x-underline:single;",
                "x-underline:single;"
        );
        assertThat(
                textUnits.get(3).getSource().getParts().get(0).getContent().getCodes()
        ).hasSize(0);
    }

    private List<Event> eventsFor(final String path, final ConditionalParameters params) throws Exception{
        final RawDocument doc = new RawDocument(
            root.in(path).asUrl().toURI(),
            StandardCharsets.UTF_8.name(),
            locENUS
        );
        return FilterTestDriver.getEvents(new OpenXMLFilter(), doc, params);
    }
}
