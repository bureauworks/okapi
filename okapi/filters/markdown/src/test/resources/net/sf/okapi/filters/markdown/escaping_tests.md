
## **Escape In Headers \(**[**View on BitBucket**](https://bitbucket.org/okapiframework/okapi)**\)**

Escape things \(that we care about\) in regular\_text.

We should disable escaping inside inline codes: `dontEscapeTheseParens(please)`.
