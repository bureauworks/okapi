package net.sf.okapi.lib.search.lucene.scorer;

import gnu.trove.map.hash.TIntIntHashMap;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;

import org.apache.lucene.index.*;
import org.apache.lucene.search.DocIdSetIterator;
import org.apache.lucene.search.Scorer;
import org.apache.lucene.search.Weight;
import org.apache.lucene.util.BitSetIterator;
import org.apache.lucene.util.FixedBitSet;

/**
 * @author HARGRAVEJE
 *
 */
public class TmFuzzyScorer extends Scorer {
	// note that the ROUGH_THRESHOLD is the lowest accepted threshold
	// TODO: externalize this
	private static float ROUGH_CUTOFF = 0.50f;

	private List<Term> terms;
	private IndexReader reader;
	private float threshold;
	private float score;
	private int currentDoc;
	private int roughThresholdFreq;
	private DocIdSetIterator docPointerIterator;
	private TIntIntHashMap scoredDocs;
	private int uniqueTermSize;
	private String termCountField;

	/**
	 * @param threshold
	 * @param weight
	 * @param terms
	 * @param reader
	 * @throws IOException
	 */
	public TmFuzzyScorer(float threshold, Weight weight,
			List<Term> terms, IndexReader reader, String termCountField) throws IOException {
		super(weight);
		this.reader = reader;
		this.threshold = threshold;
		this.terms = terms;
		this.termCountField = termCountField;
		this.scoredDocs = new TIntIntHashMap();
		this.currentDoc = -1;
	}

	private void calculateScores() throws IOException {
		// initialize buffers
		FixedBitSet docPointers = new FixedBitSet(reader.maxDoc());
		List<Term> uniqueTerms = new LinkedList<>(new LinkedHashSet<>(terms));
		uniqueTermSize = uniqueTerms.size();
		this.roughThresholdFreq = (int) (uniqueTermSize * ROUGH_CUTOFF);
		for (Iterator<Term> iter = uniqueTerms.iterator(); iter.hasNext();) {
			Term term = iter.next();
			for (LeafReaderContext lrx: reader.leaves()) {
				LeafReader lr = lrx.reader();

				PostingsEnum posts = lr.postings(term, PostingsEnum.FREQS);
				if (posts == null) continue;
				int docId;
				while ((docId = posts.nextDoc()) != DocIdSetIterator.NO_MORE_DOCS) {
					int f = scoredDocs.adjustOrPutValue(docId, 1, 1);
					if (f > roughThresholdFreq) {
						docPointers.set(docId);
					}
				}
			}
		}

		if (docPointers.cardinality() > 0) {
			docPointerIterator = new BitSetIterator(docPointers, 1);
		}
	}

	@Override
	public float score() throws IOException {
		return score;
	}

	private float calculateScore() throws IOException {
		score = (float) ((2.0f * (float) scoredDocs.get(currentDoc)) /
					(float) (reader.getTermVector(currentDoc, termCountField).size() + uniqueTermSize)) * 100.0f;

		return score;
	}

	@Override
	public int docID() {
		return currentDoc;
	}

	@Override
	public DocIdSetIterator iterator() {

		return new DocIdSetIterator() {
			@Override
			public int docID() {
				return currentDoc;
			}

			@Override
			public int nextDoc() throws IOException {
				// test for first time
				if (docPointerIterator == null) {
					calculateScores();
					if (docPointerIterator == null) {
						currentDoc = NO_MORE_DOCS;
						return NO_MORE_DOCS;
					}
				}

				while (true) {
					currentDoc = docPointerIterator.nextDoc();
					if (currentDoc == NO_MORE_DOCS) {
						return currentDoc;
					}

					if (calculateScore() >= threshold) {
						return currentDoc;
					}
				}
			}

			@Override
			public int advance(int target) throws IOException {
				if (target == NO_MORE_DOCS) {
					currentDoc = NO_MORE_DOCS;
					return NO_MORE_DOCS;
				}

				while((currentDoc = nextDoc()) < target) {
				}

				return currentDoc;
			}

			@Override
			public long cost() {
				return 0;
			}
		};
	}

	@Override
	public float getMaxScore(int i) throws IOException {
		return Float.POSITIVE_INFINITY; // Followed SpanScorer#getMaxScore(int). Not sure if this works.
	}
}
