package net.sf.okapi.common.encoder;

import net.sf.okapi.common.IParameters;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MarkdownEncoder extends DefaultEncoder {
	private final Logger LOGGER = LoggerFactory.getLogger(MarkdownEncoder.class);

	private boolean escapeSpecialCharacters;
	private String charactersToEscape;
	private IParameters params;
	public static final String DEFAULT_CHARACTERS_TO_ESCAPE = "*_`{}[]<>()#+\\-.!|";
	private Pattern escapingRegex;

	public MarkdownEncoder() {
		super();
		escapeSpecialCharacters = false;
		charactersToEscape = DEFAULT_CHARACTERS_TO_ESCAPE;
	}

	@Override
	public void setOptions(IParameters params, String encoding, String lineBreak) {
		this.params = params;
		if (params != null) {
			escapeSpecialCharacters = params.getBoolean("unescapeBackslashCharacters");
			charactersToEscape = params.getString("charactersToEscape");
			if (escapeSpecialCharacters) {
				escapingRegex = buildEscapingRegex();
			}
		}
		setLineBreak(lineBreak);
	}

	@Override
	public String encode(String text, EncoderContext context) {
		if (text == null)
			return "";

		String result = text.replace("\n", getLineBreak());

		if (escapeSpecialCharacters) {
			return result.replaceAll(escapingRegex.pattern(), "\\\\$1");
		}

		return result;
	}

	@Override
	public String encode(char value,
						 EncoderContext context) {

		String result = String.valueOf(value).replace("\n", getLineBreak());

		if (escapeSpecialCharacters) {
			return result.replaceAll(escapingRegex.pattern(), "\\\\$1");
		}

		return result;
	}

	@Override
	public String encode(int value,
						 EncoderContext context) {
		String result;
		if (Character.isSupplementaryCodePoint(value)) {
			result = new String(Character.toChars(value)).replace("\n", getLineBreak());
		} else {
			result = String.valueOf((char) value).replace("\n", getLineBreak());
		}

		if (escapeSpecialCharacters) {
			return result.replaceAll(escapingRegex.pattern(), "\\\\$1");
		}

		return result;
	}

  private Pattern buildEscapingRegex() {
		try {
			return Pattern.compile("([" + Pattern.quote(charactersToEscape) + "])");
		} catch (PatternSyntaxException e) {
			LOGGER.warn("Invalid charactersToEscape value: {}", charactersToEscape);
			return Pattern.compile("([" + Pattern.quote(DEFAULT_CHARACTERS_TO_ESCAPE) + "])");
		}
	}

	@Override
	public IParameters getParameters() {
		return params;
	}
}
